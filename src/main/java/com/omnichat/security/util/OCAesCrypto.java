package com.omnichat.security.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class consists exclusively of static methods for obtaining encryptors and decryptors for the
 * AES algorithm
 */
public class OCAesCrypto {

  public static final String DEFAULT_IV = "kt6Cy6ph5L6udKcYCEXRng==";

  private OCAesCrypto() {}

  /**
   * Return an encryptor to encrypt a String with an AES algorithm
   *
   * @param secretKey
   * @param iv
   * @return
   */
  public static Encrytor Encrytor(String secretKey, String iv) {
    return new Encrytor(secretKey, iv);
  }

  /**
   * Return a decryptor to decrypt a String with an AES algorithm
   *
   * @param secretKey
   * @param iv
   * @return
   */
  public static Decrytor Decryptor(String secretKey, String iv) {
    return new Decrytor(secretKey, iv);
  }

  /**
   * A helper method to generate an AES secret key
   *
   * @param keySize
   * @return
   * @throws NoSuchAlgorithmException
   */
  public static SecretKey generateKey(int keySize) throws NoSuchAlgorithmException {
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(keySize);

    return keyGenerator.generateKey();
  }

  /**
   * A helper method to generate an AES IV
   *
   * @return
   */
  public static IvParameterSpec generateIv() {
    byte[] iv = new byte[16];
    new SecureRandom().nextBytes(iv);
    return new IvParameterSpec(iv);
  }

  public static class Decrytor {

    private final SecretKeySpec secretKey;
    private final String iv;

    protected Decrytor(String secretKey, String iv) {
      this.secretKey = new SecretKeySpec(Base64.getDecoder().decode(secretKey), "AES");
      this.iv = iv;
    }

    /**
     * Decrypt the input Base64 String
     *
     * @param input
     * @return null if the input String's length is less than 16 or failed to decrypt
     */
    public String decryptFromBase64(String input)
        throws InvalidAlgorithmParameterException, NoSuchPaddingException,
            IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException,
            InvalidKeyException {
      byte[] decodedInput = Base64.getDecoder().decode(input);
      if (decodedInput.length < 16) {
        return null;
      }

      IvParameterSpec ivSpec = new IvParameterSpec(Base64.getDecoder().decode(iv));

      return decrypt(decodedInput, secretKey, ivSpec);
    }

    private String decrypt(byte[] input, SecretKeySpec secretKeySpec, IvParameterSpec ivSpec)
        throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
      Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      aesCipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec);

      byte[] inputBytes = Arrays.copyOfRange(input, 16, input.length);
      byte[] outputBytes = aesCipher.doFinal(inputBytes);

      return new String(outputBytes, StandardCharsets.UTF_8);
    }
  }

  public static class Encrytor {

    private final SecretKeySpec secretKey;
    private final String iv;

    protected Encrytor(String secretKey, String iv) {
      this.secretKey = new SecretKeySpec(Base64.getDecoder().decode(secretKey), "AES");
      this.iv = iv;
    }

    /**
     * Encrypt input String and return in Base64
     *
     * @param input
     * @return null if failed to encrypt
     */
    public String encryptInBase64(String input)
        throws InvalidAlgorithmParameterException, NoSuchPaddingException,
            IllegalBlockSizeException, NoSuchAlgorithmException, IOException, BadPaddingException,
            InvalidKeyException {
      IvParameterSpec ivSpec = new IvParameterSpec(Base64.getDecoder().decode(iv));

      return encrypt(input, secretKey, ivSpec);
    }

    private String encrypt(String input, SecretKeySpec secretKeySpec, IvParameterSpec ivSpec)
        throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException,
            InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
      Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      aesCipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);

      byte[] inputBytes = input.getBytes(StandardCharsets.UTF_8);
      byte[] outputBytes = aesCipher.doFinal(inputBytes);

      // iv is 128bit prefixed
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      outputStream.write(ivSpec.getIV());
      outputStream.write(outputBytes);

      return Base64.getEncoder().encodeToString(outputStream.toByteArray());
    }
  }
}
