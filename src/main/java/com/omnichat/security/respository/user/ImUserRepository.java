package com.omnichat.security.respository.user;

import com.omnichat.common.enums.user.UserStatus;
import com.omnichat.security.domain.user.ImUser;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("securityImUserRepository")
public interface ImUserRepository extends JpaRepository<ImUser, String> {

  Optional<ImUser> findByUsernameAndPasswordAndStatusNot(
      @Param("username") String username,
      @Param("password") String password,
      @Param("status") UserStatus status);
}
