package com.omnichat.security;

import com.omnichat.common.utils.YamlPropertySourceFactory;
import com.omnichat.security.filter.jwe.JweAuthenticationFailureHandler;
import com.omnichat.security.filter.jwe.JweAuthenticationFilter;
import com.omnichat.security.filter.jwe.JweAuthenticationProvider;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

/** Security configuration */
@EnableWebSecurity
@EnableJpaRepositories
@PropertySource(
    value = "classpath:security-config-${spring.profiles.active}.yml",
    factory = YamlPropertySourceFactory.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${cors.urls}")
  private List<String> corsUrls;

  @Value("${security.debug:false}")
  private boolean enableDebug;

  @SuppressWarnings({"squid:S1611", "squid:S1612"})
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    var jweAuthenticationFilter = new JweAuthenticationFilter("/**", authenticationManager());
    jweAuthenticationFilter.setAuthenticationFailureHandler(jweAuthenticationFailureHandler());

    http.csrf((crsf) -> crsf.disable())
        .formLogin((formLogin) -> formLogin.disable())
        .httpBasic((httpBasic) -> httpBasic.disable())
        .sessionManagement(
            (session) -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .cors()
        .and()
        .addFilterBefore(jweAuthenticationFilter, ExceptionTranslationFilter.class)
        .authorizeRequests()
        .anyRequest()
        .authenticated();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(jweAuthenticationProvider());
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.debug(enableDebug);
  }

  @Bean
  public JweAuthenticationProvider jweAuthenticationProvider() {
    return new JweAuthenticationProvider();
  }

  @Bean
  public JweAuthenticationFailureHandler jweAuthenticationFailureHandler() {
    return new JweAuthenticationFailureHandler();
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    return request -> {
      var corsConfig = new CorsConfiguration();
      corsConfig.setAllowedOriginPatterns(corsUrls);

      return corsConfig;
    };
  }
}
