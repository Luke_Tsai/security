package com.omnichat.security.filter.jwe;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omnichat.responsehandler.error.dto.ErrorResponse;
import com.omnichat.responsehandler.error.enums.DefaultErrorCode;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class JweAuthenticationFailureHandler implements AuthenticationFailureHandler {

  private static final Logger logger =
      LoggerFactory.getLogger(JweAuthenticationFailureHandler.class);

  @Override
  public void onAuthenticationFailure(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
      throws IOException, ServletException {
    logger.info("Failed to authenticate the user", exception);

    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    response.setStatus(HttpStatus.UNAUTHORIZED.value());
    response.setCharacterEncoding(StandardCharsets.UTF_8.name());
    Writer out = response.getWriter();
    ErrorResponse errorResponse =
        new ErrorResponse(DefaultErrorCode.AUTHENTICATION_ERROR, UUID.randomUUID().toString());
    ObjectMapper objectMapper = new ObjectMapper();
    out.write(objectMapper.writeValueAsString(errorResponse));
    out.flush();
  }
}
