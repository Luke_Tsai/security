package com.omnichat.security.filter.jwe;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.omnichat.common.dto.user.ImmutableOCUser;
import com.omnichat.common.enums.user.UserRole;
import com.omnichat.common.enums.user.UserStatus;
import com.omnichat.security.domain.user.ImUser;
import com.omnichat.security.respository.user.ImUserRepository;
import com.omnichat.security.util.OCAesCrypto;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class JweAuthenticationProvider implements AuthenticationProvider {

  private static final String USERNAME_KEY = "e";
  private static final String PASSWORD_KEY = "d";
  private static final String TEAM_KEY = "t";
  private static final String IS_ADMIN_KEY = "isAdmin";
  private static final String IS_ANONYMOUSE_KEY = "isAnonymous";
  private static final String PROFILE_PATH_KEY = "p";
  private static final String ROLE_KEY = "role";
  private static final String SSO_KEY = "sso";
  private static final String METHOD_KEY = "method";

  @Autowired
  @Qualifier("securityImUserRepository")
  private ImUserRepository imUserRepository;

  @Value("${jweSecretKey}")
  private String jweSecretKey;

  @Value("${aesKey}")
  private String aesKey;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(jweSecretKey)).build();
    try {
      DecodedJWT decodedJWT = jwtVerifier.verify((String) authentication.getCredentials());
      if (decodedJWT == null || decodedJWT.getClaims() == null) {
        throw new BadCredentialsException("Cannot decrypt the token");
      }

      Map<String, Claim> claims = decodedJWT.getClaims();
      OCAesCrypto.Decrytor decrytor = OCAesCrypto.Decryptor(aesKey, OCAesCrypto.DEFAULT_IV);
      String username = decrytor.decryptFromBase64(claims.get("e").asString());

      var rawPassword = claims.get("d") != null ? claims.get("d").asString() : null;

      Optional<ImUser> imUserOptional =
          imUserRepository.findByUsernameAndPasswordAndStatusNot(
              username, rawPassword, UserStatus.NONVALIDATED);
      if (imUserOptional.isEmpty()) {
        throw new BadCredentialsException("Bad credentials");
      }

      UserRole role =
          claims.get("role") != null
              ? UserRole.fromCode(claims.get(ROLE_KEY).asInt()).get()
              : UserRole.GUEST;

      return new UsernamePasswordAuthenticationToken(
          ImmutableOCUser.builder()
              .username(decrytor.decryptFromBase64(claims.get(USERNAME_KEY).asString()))
              .password(
                  claims.get(PASSWORD_KEY) != null
                      ? decrytor.decryptFromBase64(claims.get(PASSWORD_KEY).asString())
                      : null)
              .team(claims.get("t") != null ? claims.get(TEAM_KEY).asString() : null)
              .admin(Boolean.valueOf(claims.get(IS_ADMIN_KEY).asString()))
              .anonymous(Boolean.valueOf(claims.get(IS_ANONYMOUSE_KEY).asString()))
              .profilePath(claims.get("p") != null ? claims.get(PROFILE_PATH_KEY).asString() : null)
              .sSO(
                  claims.get("sso") != null
                      ? Boolean.valueOf(claims.get(SSO_KEY).toString())
                      : Boolean.FALSE)
              .role(role.getCode())
              .email(imUserOptional.get().getEmail())
              .method(claims.get(METHOD_KEY) != null ? claims.get(METHOD_KEY).toString() : null)
              .build(),
          null,
          List.of(new SimpleGrantedAuthority(role.getName())));
    } catch (Exception e) {
      throw new AuthenticationServiceException(e.getMessage(), e);
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }
}
