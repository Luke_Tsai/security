package com.omnichat.security.filter.jwe;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

public class JweAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  public JweAuthenticationFilter(
      String defaultFilterProcessesUrl, AuthenticationManager authenticationManager) {
    super(defaultFilterProcessesUrl, authenticationManager);
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    var authorization = request.getHeader("Authorization");
    if (StringUtils.isBlank(authorization)) {
      throw new BadCredentialsException("No Authorization header");
    }

    var bearer = authorization.split(" ");
    if (!"Bearer".equals(bearer[0])) {
      throw new BadCredentialsException("No Bearer header");
    }

    var token = bearer[1];
    if (StringUtils.isBlank(token)) {
      throw new BadCredentialsException("No Bearer header");
    }

    return getAuthenticationManager()
        .authenticate(new UsernamePasswordAuthenticationToken(null, token));
  }

  @Override
  protected void successfulAuthentication(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain chain,
      Authentication authResult)
      throws IOException, ServletException {
    // Set the authentication to the SecurityContext for the further authorization
    SecurityContext context = SecurityContextHolder.getContext();
    context.setAuthentication(authResult);
    chain.doFilter(request, response);
  }
}
