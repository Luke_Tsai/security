package com.omnichat.security.domain.user;

import com.omnichat.common.enums.user.UserRole;
import com.omnichat.common.enums.user.UserStatus;
import com.omnichat.common.utils.OCStringUtils;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "imUser")
public class ImUser {

  @Id
  @Column(name = "username")
  private String username;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "client_team")
  private String clientTeam;

  @Column(name = "team")
  private String team;

  @Column(name = "name")
  private String name;

  @Column(name = "phone")
  private String phone;

  @Column(name = "pw_changed")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private boolean isPWChanged = false;

  @Column(name = "ipAddress")
  private String ipAddress = "0";

  @Column(name = "country_name")
  private String countryName = null;

  @Column(name = "city_name")
  private String cityName = null;

  @Column(name = "profile_path")
  private String profilePath;

  @Column(name = "creationDate")
  private String creationDate;

  @Column(name = "modificationDate")
  private String modificationDate;

  @Column(name = "status")
  private UserStatus status = UserStatus.VALIDATED;

  @Column(name = "isAdmin")
  private Boolean isAdmin = false;

  @Column(name = "isAnonymous")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean isAnonymous = false;

  @Column(name = "isFbUser")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean isFbUser = false;

  @Column(name = "fbPageId")
  private String fbPageId;

  @Column(name = "isLineUser")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean isLineUser = false;

  @Column(name = "lineChannelId")
  private String lineChannelId;

  @Column(name = "partnerMemberId")
  private String partnerMemberId;

  @Column(name = "enable_all_notification")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean enableAllNotification = true;

  @Column(name = "autoReplyMessage")
  private String autoReplyMessage;

  @Column(name = "autoReplyEnabled")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean autoReplyEnabled = false;

  @Column(name = "appDownloaded")
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean appDownloaded = false;

  @Column(name = "role", nullable = false, columnDefinition = "int default 0")
  private UserRole role = UserRole.GUEST;

  @Column(name = "clientId")
  private String clientId;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getClientTeam() {
    return clientTeam;
  }

  public void setClientTeam(String clientTeam) {
    this.clientTeam = clientTeam;
  }

  public String getTeam() {
    return team;
  }

  public void setTeam(String team) {
    this.team = team;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public boolean isPWChanged() {
    return isPWChanged;
  }

  public void setPWChanged(boolean isPWChanged) {
    this.isPWChanged = isPWChanged;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getProfilePath() {
    return profilePath;
  }

  public void setProfilePath(String profilePath) {
    this.profilePath = profilePath;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getModificationDate() {
    return modificationDate;
  }

  public void setModificationDate(String modificationDate) {
    this.modificationDate = modificationDate;
  }

  public UserStatus getStatus() {
    return status;
  }

  public void setStatus(UserStatus status) {
    this.status = status;
  }

  public Boolean getIsAdmin() {
    return isAdmin;
  }

  public void setIsAdmin(Boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public Boolean getIsAnonymous() {
    return isAnonymous;
  }

  public void setIsAnonymous(Boolean isAnonymous) {
    this.isAnonymous = isAnonymous;
  }

  public Boolean getIsFbUser() {
    return isFbUser;
  }

  public void setIsFbUser(Boolean isFbUser) {
    this.isFbUser = isFbUser;
  }

  public String getFbPageId() {
    return fbPageId;
  }

  public void setFbPageId(String fbPageId) {
    this.fbPageId = fbPageId;
  }

  public Boolean getIsLineUser() {
    return isLineUser;
  }

  public void setIsLineUser(Boolean isLineUser) {
    this.isLineUser = isLineUser;
  }

  public String getLineChannelId() {
    return lineChannelId;
  }

  public void setLineChannelId(String lineChannelId) {
    this.lineChannelId = lineChannelId;
  }

  public String getPartnerMemberId() {
    return partnerMemberId;
  }

  public void setPartnerMemberId(String partnerMemberId) {
    this.partnerMemberId = partnerMemberId;
  }

  public Boolean getEnableAllNotification() {
    return enableAllNotification;
  }

  public void setEnableAllNotification(Boolean enableAllNotification) {
    this.enableAllNotification = enableAllNotification;
  }

  public String getAutoReplyMessage() {
    return autoReplyMessage;
  }

  public void setAutoReplyMessage(String autoReplyMessage) {
    this.autoReplyMessage = autoReplyMessage;
  }

  public Boolean getAutoReplyEnabled() {
    return autoReplyEnabled;
  }

  public void setAutoReplyEnabled(Boolean autoReplyEnabled) {
    this.autoReplyEnabled = autoReplyEnabled;
  }

  public Boolean getAppDownloaded() {
    return appDownloaded;
  }

  public void setAppDownloaded(Boolean appDownloaded) {
    this.appDownloaded = appDownloaded;
  }

  public Boolean getIsWhatsAppUser() {
    return this.username.startsWith("wh");
  }

  /*
   *  Return the creation date with time zone
   */
  public String getZonedCreationDate(String timezone) {
    Date creationDate = new Date(Long.parseLong(this.creationDate));
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
    return simpleDateFormat.format(creationDate);
  }

  /*
   *  Return the modification date with time zone
   */
  public String getZonedModificationDate(String timezone) {
    Date creationDate = new Date(Long.parseLong(this.modificationDate));
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
    simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
    return simpleDateFormat.format(creationDate);
  }

  public UserRole getRole() {
    return role;
  }

  public void setRole(UserRole role) {
    this.role = role;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public Boolean getIsOmnichatUser() {
    return !OCStringUtils.isNullOrEmpty(this.email)
        || !OCStringUtils.isNullOrEmpty(this.partnerMemberId)
        || !OCStringUtils.isNullOrEmpty(this.phone);
  }
}
